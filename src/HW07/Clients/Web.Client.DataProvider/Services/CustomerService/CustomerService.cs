using Bogus;

using Domain.Entities;

namespace Web.Client.DataProvider.Services.CustomerService
{
    public class CustomerService
    {
        public Task<List<Customer>>? GetAllAsync()
        {
            return null;
        }
        public Customer Create(long id)
        {
            var customer = NewRandom();
            customer.Id = id;
            return customer;
        }
        private static Customer NewRandom()
        {
            var fakeCustomer = new Faker<Customer>()
                .RuleFor(u => u.Firstname, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.Lastname, (f, u) => f.Name.LastName());

            var customer = fakeCustomer.Generate();
            return customer;
        }
    }
}
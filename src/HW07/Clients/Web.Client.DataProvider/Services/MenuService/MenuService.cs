using System.Text.Json;
using System.Text.Json.Serialization;

using Domain.Entities;

namespace Web.Client.DataProvider.Services.MenuService
{
    public class MenuService
    {
        public bool continueFlag { get; set; } = true;
        string? _consoleKey { get; set; } = string.Empty;
        List<string> _menu { get; set; }
        const string ShowCustomerKey = "C";
        const string RandomCustomerKey = "R";
        const string ExitKey = "E";
        CustomerService.CustomerService customerService = new CustomerService.CustomerService();
        public MenuService()
        {
            _menu = new List<string>
                {
                    $"Для показа таблицы Пользователей наберите {ShowCustomerKey} -  ",
                    $"Для генерации случайного пользователя наберите {RandomCustomerKey} -  ",
                    $"Для выхода наберите {ExitKey} -  ",
                };
        }

        public void Show()
        {
            _menu.ForEach(item => Console.WriteLine($"{item}"));
        }

        public void Clear()
        {
            Console.Clear();
        }

        public async Task ReadConsoleInput()
        {
            HttpResponseMessage response = new();
            _consoleKey = Console.ReadLine();
            var consoleKeyUpper = _consoleKey?.ToUpper();

            if (!string.IsNullOrEmpty(consoleKeyUpper))
            {
                if (consoleKeyUpper == ExitKey)
                {
                    continueFlag = false;
                }

                if (consoleKeyUpper == ShowCustomerKey)
                {
                    Console.WriteLine("Таблица пользователей: ");
                    response = await SendService.SendService.GetAllCustomers();
                    var statusCode = response.StatusCode;
                    var contentStream = await response.Content.ReadAsStreamAsync();
                    var customers = JsonSerializer.Deserialize<List<Customer>>(contentStream, new JsonSerializerOptions { DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull, PropertyNameCaseInsensitive = true });
                    customers?.ForEach(item => Console.WriteLine($"Id - {item.Id}, Firstname - {item.Firstname}, Lastname - {item.Lastname}"));
                    Console.WriteLine("Для возврата в основное меню нажмите Enter...");
                    Console.ReadLine();
                }

                if (consoleKeyUpper == RandomCustomerKey)
                {
                    Console.WriteLine("Создание случайного пользователя...");
                    Console.WriteLine("Введите предполагаемый идентификатор пользователя (от 1 и выше): ");
                    var input = Console.ReadLine();
                    try
                    {
                        if (input?.Trim() != null)
                        {
                            bool success = Int64.TryParse(input, out long result);
                            if (success && result >= 1)
                            {
                                var customer = customerService.Create(result);
                                response = await SendService.SendService.CreateCustomer(customer);
                                var statusCode = response.StatusCode;
                                var contentStream = await response.Content.ReadAsStreamAsync();
                                var newCustomer = JsonSerializer.Deserialize<Customer>(contentStream, new JsonSerializerOptions { DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull, PropertyNameCaseInsensitive = true });
                                Console.WriteLine($"Код - {statusCode}, Id - {newCustomer?.Id}, Пользователь - {newCustomer?.Firstname} - {newCustomer?.Lastname};");
                                Console.WriteLine("Для продолжения нажмите Enter...");
                                Console.ReadLine();
                            }
                            else
                            {
                                Console.WriteLine("Введено некорректное значение");
                                Console.WriteLine("Для возврата в основное меню нажмите Enter...");
                                Console.ReadLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Введено некорректное значение");
                            Console.WriteLine("Для возврата в основное меню нажмите Enter...");
                            Console.ReadLine();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine("Для возврата в основное меню нажмите Enter...");
                        Console.ReadLine();
                    }
                }
            }
        }
    }
}
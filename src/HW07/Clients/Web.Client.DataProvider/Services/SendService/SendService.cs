using System.Text;

using Domain.Entities;

namespace Web.Client.DataProvider.Services.SendService
{
    public class SendService
    {
        public SendService()
        {

        }

        public static async Task<HttpResponseMessage> CreateCustomer(Customer customer)
        {
            HttpResponseMessage response = new();

            try
            {
                var url = "http://localhost:5269/api/customer";
                var client = new HttpClient();

                var body = System.Text.Json.JsonSerializer.Serialize(customer);
                var request = new StringContent(body, Encoding.UTF8, "application/json");
                response = await client.PostAsync(url, request);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return response;
        }
        public static async Task<HttpResponseMessage> GetAllCustomers()
        {
            HttpResponseMessage response = new();

            try
            {
                var url = "http://localhost:5269/api/customer";
                var client = new HttpClient();

                response = await client.GetAsync(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return response;
        }
    }
}
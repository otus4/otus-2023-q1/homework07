﻿using Web.Client.DataProvider.Services.MenuService;

var menu = new MenuService();

do
{
    menu.Clear();
    menu.Show();
    await menu.ReadConsoleInput();
} while (menu.continueFlag);
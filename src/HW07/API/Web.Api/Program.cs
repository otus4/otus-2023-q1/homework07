using Web.Api.DataProvider.Repositories;
using Web.Api.DataProvider.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// var connectionString = builder.Configuration.GetConnectionString("DefaultConnection") ?? "Server=hw07dbsrv; Database=hw07db; Port=5432; User Id=admin; Password=admin1234;";

builder.Services.AddScoped<ICustomerRepository, CustomerRepository>(); //(r => new CustomerRepository(connectionString));
builder.Services.AddScoped<ICustomerService, CustomerService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

#region Создание таблиц в БД
using var scope = app.Services.CreateScope();
var provider = scope.ServiceProvider;
try
{
    var customerRepository = provider.GetRequiredService<ICustomerRepository>();
    customerRepository.CreateTables();
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
#endregion

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();

using Application.Features.Customer;

using Domain.Entities;

using Microsoft.AspNetCore.Mvc;

using Web.Api.DataProvider.Services;

namespace Web.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public async Task<ActionResult<IList<Customer>>> GetCustomers()
        {

            var customers = await _customerService.GetAllCustomersAsync();
            return Ok(customers);
        }

        [HttpGet("{id:long}")]
        public async Task<ActionResult> GetCustomerByIdAsync([FromRoute] GetCustomerById dto)
        {
            var newCustomer = await _customerService.GetCustomer(dto.Id);
            return Ok(newCustomer);
        }

        [HttpPost]
        public async Task<ActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            var newCustomer = await _customerService.CreateCustomer(customer);
            return Ok(newCustomer);
        }
    }
}
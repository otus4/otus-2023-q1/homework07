using Domain.Entities;

using Web.Api.DataProvider.Repositories;

namespace Web.Api.DataProvider.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<Customer> CreateCustomer(Customer customer)
        {
            await _customerRepository.CreateCustomer(customer);
            var newCustomer = await _customerRepository.GetCustomerByName(customer.Firstname, customer.Lastname);
            return newCustomer;
        }

        public async Task<IList<Customer>> GetAllCustomersAsync()
        {
            var customers = await _customerRepository.GetAllCustomersAsync();
            return customers;
        }

        public async Task<Customer> GetCustomer(long id)
        {
            var customer = await _customerRepository.GetCustomer(id);
            return customer;
        }
    }
}
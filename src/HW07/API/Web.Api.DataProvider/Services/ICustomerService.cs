using Domain.Entities;

namespace Web.Api.DataProvider.Services
{
    public interface ICustomerService
    {
        Task<IList<Customer>> GetAllCustomersAsync();
        Task<Customer> CreateCustomer(Customer customer);
        Task<Customer> GetCustomer(long id);
    }
}
using Domain.Entities;

namespace Web.Api.DataProvider.Repositories
{
    public interface ICustomerRepository
    {
        void CreateTables();
        Task<IList<Customer>> GetAllCustomersAsync();
        Task CreateCustomer(Customer customer);
        Task<Customer> GetCustomer(long id);
        Task<Customer> GetCustomerByName(string firstname, string lastname);
    }
}
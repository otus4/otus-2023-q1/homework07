using Domain.Entities;

using Npgsql;

namespace Web.Api.DataProvider.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly NpgsqlConnection _connection;
        private readonly string _connectionString;

        public CustomerRepository()
        {
            _connectionString = "Server=localhost; Database=hw07db; Port=5432; User Id=admin; Password=admin1234;";
            var dataSource = NpgsqlDataSource.Create(_connectionString);
            _connection = dataSource.OpenConnection();
        }

        public void CreateTables()
        {
            var cmd = _connection.CreateCommand();

            cmd.CommandText = @"CREATE TABLE IF NOT EXISTS ""Customers"" (
                ""Id"" bigint GENERATED ALWAYS AS IDENTITY,
                ""Firstname"" text NOT NULL,
                ""Lastname"" text  NOT NULL,
                CONSTRAINT ""PK_Customers"" PRIMARY KEY(""Id""))";
            cmd.ExecuteNonQuery();
        }

        public async Task<IList<Customer>> GetAllCustomersAsync()
        {
            var result = new List<Customer>();
            var cmd = _connection.CreateCommand();
            cmd.CommandText = @"SELECT ""Id"", ""Firstname"", ""Lastname"" FROM ""Customers""";

            var reader = await cmd.ExecuteReaderAsync();

            while (reader.Read())
            {
                result.Add(new Customer { Id = reader.GetInt64(0), Firstname = reader.GetString(1), Lastname = reader.GetString(2) });
            }

            reader.Close();

            return result;
        }

        public async Task CreateCustomer(Customer customer)
        {
            try
            {
                string firstname = customer.Firstname;
                string lastname = customer.Lastname;
                var cmd = _connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO ""Customers"" (""Firstname"", ""Lastname"") VALUES (:firstname, :lastname)";
                cmd.Parameters.Add(new NpgsqlParameter("Firstname", firstname));
                cmd.Parameters.Add(new NpgsqlParameter("Lastname", lastname));
                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка - {ex.Message}");
            }
        }

        public async Task<Customer> GetCustomer(long id)
        {
            var result = new Customer();
            var cmd = _connection.CreateCommand();
            cmd.CommandText = $@"SELECT ""Id"", ""Firstname"", ""Lastname"" FROM ""Customers"" WHERE ""Id"" ={id}";

            var reader = await cmd.ExecuteReaderAsync(System.Data.CommandBehavior.SingleRow);

            if (reader.Read())
            {
                result = new Customer { Id = reader.GetInt64(0), Firstname = reader.GetString(1), Lastname = reader.GetString(2) };
            }

            reader.Close();

            return result;
        }

        public async Task<Customer> GetCustomerByName(string firstname, string lastname)
        {
            var result = new Customer();
            var cmd = _connection.CreateCommand();
            cmd.CommandText = $@"SELECT ""Id"", ""Firstname"", ""Lastname"" FROM ""Customers"" WHERE ""Firstname""='{firstname}' AND ""Lastname""='{lastname}'";

            var reader = await cmd.ExecuteReaderAsync(System.Data.CommandBehavior.SingleRow);

            if (reader.Read())
            {
                result = new Customer { Id = reader.GetInt64(0), Firstname = reader.GetString(1), Lastname = reader.GetString(2) };
            }

            reader.Close();

            return result;
        }
    }
}
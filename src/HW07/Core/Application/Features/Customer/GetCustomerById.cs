using System.ComponentModel.DataAnnotations;

namespace Application.Features.Customer
{
    public class GetCustomerById
    {
        [Required]
        public long Id { get; init; }
    }
}
using System.ComponentModel.DataAnnotations;
#nullable disable

namespace Application.Features.Customer
{
    public class CreateCustomer
    {
        [Required]
        public string Firstname { get; init; }
        [Required]
        public string Lastname { get; init; }
    }
}
using Domain.Common;

#nullable disable

namespace Domain.Entities
{
    public class Customer : BaseEntity<long>
    {
        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}
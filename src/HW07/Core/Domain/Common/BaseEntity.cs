using System.ComponentModel.DataAnnotations;

using Domain.Common.Interfaces;

#nullable disable

namespace Domain.Common
{
    public class BaseEntity<TKey> : IBaseEntity where TKey : IEquatable<TKey>
    {
        [Key]
        public TKey Id { get; set; }
    }
}
@echo off
REM Стартовый шаблон инициализации решения
@chcp 65001 >> nul

@REM Задаем имя решения
set SolutionName=HW07
@if exist %SolutionName% goto exitError

REM Инициализируем решение
dotnet new sln -o %SolutionName% -n %SolutionName%
dotnet new gitignore -o %SolutionName%
dotnet new editorconfig -o %SolutionName%

cd %SolutionName%
mkdir API
mkdir Core
mkdir Clients

REM Инициализируем библиотеку инфраструктурного кода
dotnet new classlib -n Domain -o Core\Domain
dotnet new classlib -n Application -o Core\Application
dotnet new classlib -n Infrastructure -o Core\Infrastructure

REM Инициализируем проект сервера
dotnet new webapi --no-https -n Web.Api -o API\Web.Api
dotnet new classlib -n Web.API.DataProvider -o API\Web.Api.DataProvider

REM Инициализируем проект клиента
dotnet new console -n Web.Client -o Clients\Web.Client
dotnet new classlib -n Web.Client.DataProvider -o Clients\Web.Client.DataProvider

REM - Добавляем проекты в решение
dotnet sln add .\Core\Domain\ .\Core\Application\ .\Core\Infrastructure\ .\API\Web.Api .\API\Web.Api.DataProvider .\Clients\Web.Client\ .\Clients\Web.Client.DataProvider\

REM Добавляем необходимые ссылки
dotnet add .\Core\Application\ reference .\Core\Domain\
dotnet add .\Core\Infrastructure\ reference .\Core\Application\
dotnet add .\API\Web.Api.DataProvider\ reference .\Core\Infrastructure\
dotnet add .\API\Web.Api\ reference .\API\Web.Api.DataProvider\
dotnet add .\Clients\Web.Client.DataProvider\ reference .\Core\Infrastructure\
dotnet add .\Clients\Web.Client\ reference .\Clients\Web.Client.DataProvider\



REM Устанваливаем внешние зависимости
dotnet add .\Core\Infrastructure\ package Npgsql.EntityFrameworkCore.PostgreSQL
dotnet add .\Clients\Web.Client.DataProvider\ package Bogus


REM - обновление зависимостей решения
dotnet restore

@echo off
goto exitNormal
:exitError
@echo on
@echo Директория с именем решения уже существует!
@echo off
:exitNormal
@echo Инициализация решения закончена.
exit